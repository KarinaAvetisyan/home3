<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="RoomStays">
           <Hotels> <xsl:apply-templates/></Hotels>
    </xsl:template>

    <xsl:template match="RoomStay">
        <Hotel><xsl:apply-templates/></Hotel>
    </xsl:template>

    <xsl:template match="RoomRates">
        <Offers><xsl:apply-templates/></Offers>
    </xsl:template>

    <xsl:template match="RoomRate">
        <Offer><xsl:apply-templates/></Offer>
    </xsl:template>

    <xsl:template match="@AmountAfterTax">
        <xsl:attribute name="AmountAfterTax">
            <xsl:value-of select="ceiling(number(.))"/>
        </xsl:attribute>
    </xsl:template>

</xsl:stylesheet>