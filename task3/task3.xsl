<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" indent="yes"/>
    <xsl:key name="src" match="item" use="substring(string(@Name[.]),1,1)"/>

    <xsl:template match="list">
        <list>
            <xsl:apply-templates select="item[generate-id(.) = generate-id(key('src', substring(@Name[.],1,1)))]">
                <xsl:sort select="@Name" data-type="text"/>
            </xsl:apply-templates>
        </list>
    </xsl:template>

    <xsl:template match="item">
        <capital  value="{substring(@Name[.],1,1)}">
            <xsl:for-each select="key('src', substring(@Name[.],1,1))">
                <name>
                    <xsl:value-of select="key('src', substring(@Name[.],1,1))/@Name"/>
                </name>
            </xsl:for-each>
        </capital>
    </xsl:template>
</xsl:stylesheet>