<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:variable name="delimiterSecond" select="'/'"/>
    <xsl:variable name="delimiterFirst" select="'|'"/>

    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <Guests>
            <xsl:call-template name="recursion">
                <xsl:with-param name="stringToSplit" select="//guest[text()]"/>
            </xsl:call-template>
        </Guests>
    </xsl:template>

    <xsl:template name="recursion">
        <xsl:param name="stringToSplit"/>
        <xsl:param name="age" select="substring-before(substring-after(substring-before($stringToSplit,$delimiterFirst),$delimiterSecond),$delimiterSecond)"/>
        <xsl:param name="nationality" select="substring-before(substring-after(substring-after(substring-before($stringToSplit,$delimiterFirst),$delimiterSecond),$delimiterSecond),$delimiterSecond)"/>
        <xsl:param name="gender" select="substring-before(substring-after(substring-after(substring-after(substring-before($stringToSplit,$delimiterFirst),$delimiterSecond),$delimiterSecond),$delimiterSecond),$delimiterSecond)"/>
        <xsl:param name="name" select="substring-before(substring-after(substring-after(substring-after(substring-after(substring-before($stringToSplit,$delimiterFirst),$delimiterSecond),$delimiterSecond),$delimiterSecond),$delimiterSecond),$delimiterSecond)"/>
        <xsl:param name="type" select="substring-before(substring-after($stringToSplit,$delimiterSecond),$delimiterSecond)"/>
        <xsl:param name="address" select="substring-after(substring-after(substring-after(substring-after(substring-after(substring-before($stringToSplit,$delimiterFirst),$delimiterSecond),$delimiterSecond),$delimiterSecond),$delimiterSecond),$delimiterSecond)"/>

        <xsl:if test="string-length($stringToSplit) != 0">
            <Guest Age="{$age}" Nationalty="{$nationality}" Gender="{$gender}" Name="{$name}">
                <Type>
                    <xsl:value-of select="$type"/>
                </Type>
                <Profile>
                    <Address>
                        <xsl:value-of select="$address"/>
                    </Address>
                </Profile>
            </Guest>
            <xsl:call-template name="recursion">
                <xsl:with-param name="stringToSplit" select="substring-after($stringToSplit,$delimiterFirst)"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>